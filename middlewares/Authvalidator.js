const Joi = require('joi');
const validators = {};
validators.campaignReport =  Joi.object({
                                from: Joi.required(),
                                to: Joi.required(),
                            });
validators.createUser =  Joi.object().keys({
                                email: Joi.string().email().required(),
                                name: Joi.object().keys({
                                    firstName:Joi.string().required().messages({
                                        'string.empty': `firstName cannot be an empty field`,
                                    }),
                                    lastName:Joi.string().required().messages({
                                        'string.empty': `lastName cannot be an empty field`,
                                    })
                                }),
                                role_id:Joi.string().required().messages({'string.empty': `please select a role`}),
                                address:Joi.optional(),
                                gst_no:Joi.optional(),
                                company_details:Joi.optional(),
                                password:Joi.string()
                                .regex(RegExp(/^(?!.*\s)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹])/)) // you have to put it in this way and it will work :)
                                .required()
                                .min(8)
                                .max(20)
                                .messages({
                                    'string.pattern.base': `"password" must be strong, At least one upper case alphabet, At least one lower case alphabet, At least one special character, At least one number`,
                                    'string.empty': `"password" cannot be an empty field`,
                                    'string.min': `"password" should have a minimum length of {#limit}`,
                                    'any.required': `"password" is a required field`
                                  }),
                                contact_no:Joi.optional(),
                                // address:Joi.optional(),
                                // address:Joi.optional(),
                                // address:Joi.optional(),
                                // reference_no:Joi.string().allow(null, ''),
                                // record_id:Joi.string().allow(null, '')
});
validators.c2cReport =  Joi.object({
                                from: Joi.required(),
                                to: Joi.required(),
                            });
export default validators;
