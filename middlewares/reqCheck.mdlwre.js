import { UserAuthcheck } from "../controllers/auth.ctrl";

export const requestCheck = async (req,res,next)=>{
    try {
        const access_token = req.headers.authorization.split(" ");
        const response = await UserAuthcheck(access_token[1]);
        if(typeof response == "boolean"){
            if(response)
                next();
            else
                throw new Error (`Please Login Again Token Expired`);
        }else{
            throw new Error (`Something went wrong in auth check`);
        }
    }catch (error) {
          res.status(401).json({message:error.message});
    }
}