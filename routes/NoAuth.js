import express from 'express';
const router = express.Router();
import {register,login,refreshToken} from '../controllers/auth.ctrl';


/* auth routes*/
router.post("/register", register);
router.post("/login", login);
router.post("/refreshToken", refreshToken);


module.exports = router;