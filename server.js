import express from 'express';
// import {readdirSync} from "fs";
import cors from 'cors';
import { HandleError } from './errorHandler/ErrorHandler';
import { requestCheck } from './middlewares/reqCheck.mdlwre';
const mongoose = require('mongoose');
const morgan = require("morgan");
require('dotenv').config();
const app = express();

//middlewares
app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(function (req, res, next) {  
    res.header("X-powered-by", 'curosity is a curse');
    next();
  });
mongoose.connect(process.env.DATABASE,{})
.then(() => console.log('connected to db'))
.catch((err) => console.log('error',err));
app.use('/auth',require(`./routes/NoAuth`));
app.use(requestCheck);
app.use('/api', require(`./routes/api`));
app.use(HandleError);
const port = process.env.PORT || 8006;

app.listen(port,() => console.log(`server runing on port ${port}`));