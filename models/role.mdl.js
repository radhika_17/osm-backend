import mongoose from 'mongoose';
const {Schema} = mongoose;

const roleSchema = new Schema({
	name: {
        type: String,
    },
},{timestamps: {
    createdAt: 'created_at',
    updatedAt: 'updated_at'
  }});
export default mongoose.model("Roles",roleSchema)