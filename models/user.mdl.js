import mongoose from 'mongoose';
const {Schema} = mongoose;
import bcrypt from "bcrypt";

const userSchema = new Schema({
	name: {
        firstName: {
            type: String,
            // required: true,
        },
        lastName: {
            type: String,
            // required: true,
        },
    },
	address:{
		loc: {
            type: String,
        },
        city: {
            type: String,
        },
		address:{
			type:String
		}
	},
	company_details:{
		gst_no:{
			type:String,
			// required:true,
		},
		name:{
			type:String,
			// required:true,
		}
	},
	password:{
		type:String,
		// required:true,
	},
    phone_no:{
        type:Number,
		required:true,
        unique: [true, 'That phone no. is already taken.']
    },
	role_id:{
        type:Schema.Types.ObjectId,
		ref:"Roles"
    },
	CreatedBy: {
        type: Schema.Types.ObjectId,
        ref: "User",
    },
});

//hashing password;

userSchema.pre('save',function(next){
	let user = this;
	if(user.isModified('password')){
		return bcrypt.hash(user.password,12,function(err,hash){
			if(err){
				console.log('BCRYPT HAS ERR');
				return next(err);
			}
			user.password = hash;
			return next();
		});
	}
	else{
		return next();
	}
});

userSchema.methods.comparePassword = function(password,next){
	bcrypt.compare(password,this.password,function(err,match){
		if(err){
			next(err,false);
		}
		if(match)
			return next(null,match);
		else
			return next(null,match);
	});

};

export default mongoose.model("Users",userSchema)