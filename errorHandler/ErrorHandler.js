import { logger } from "./WinstonErrorLogger";
export const HandleError = async (error,req,res,next)=>{
    let status = 500;
    if(error.hasOwnProperty('status')){
        status = error.status;
    }
    logger.error(error.stack);
    res.status(status).json({message:'server error'});
}