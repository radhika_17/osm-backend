import Roles from "../models/role.mdl";
import Users from "../models/user.mdl";
import validators from "../middlewares/Authvalidator";
import jwt from "jsonwebtoken";
import bcrypt from "bcrypt";
import { logger } from "../errorHandler/WinstonErrorLogger";
export const register = async (req,res) => {
    const { name, password, phone_no } = req.body;
	if(!name) return res.status(400).send("Name is required");
	if(!password || password.length < 6) return res.status(400).send("password is required or atleast 6 character long");
    let userExist = await User.findOne({ phone_no }).exec();
	if(userExist) 
        return res.status(400).json({
            message:"phone no. already taken"
        });
    const user = new User(req.body)
    try{
        await user.save();
        console.log("user created",user);
        return res.status(200).json({
            ok:true
        });
    }
    catch(err){
        console.log(err);
        res.status(400).send("err creating try again");
    }
};
export const login = async (req,res,next) => {
    try{
        const { phone_no,Password} = req.body;
        let user = await Users.findOne({ phone_no: phone_no }).populate("role_id").exec();
        if(!user) return res.status(400).json({message:'user not found'});
            bcrypt.compare(Password,user.password,function(err,match){
                if(!match || err) 
                return res.status(400).json({message:'Wrong Password'});
                let authToken = jwt.sign({_id:user._id,role:user.role_id.name},process.env.JWT_SECRET,{expiresIn: '1h'});
                let refreshToken = jwt.sign({_id:user._id},process.env.JWT_SECRET,{expiresIn: '12h'});
                let now = new Date;
                return res.status(200).json({authToken,refreshToken,expiresIn:new Date(now.getDate() + 7),roleName:user.role_id.name});
            });
    }
    catch(err){
        console.log("err creating user",err);
        next(err)
    }
};
export const GetUserDetails = async (req,res,next)=>{
    try {
        const {authorization} = req.headers;
        const token = authorization.split(" ")[1];
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        let user = await Users.findById(decoded._id).populate("role_id");
        res.status(200).json({userData:{
            id:user._id,
            firstName:user.name.firstName,
            lastName:user.name.lastName,
            phone_no:user.phone_no,
            role:user.role_id.name
        }});
    } catch (error) {
        console.log(error);
        next(error)
    }
}
export const UserAuthcheck = async (token)=>{
    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        var currentTimestamp = new Date().getTime() / 1000;
        var IstokenNotExpired =  decoded.exp > currentTimestamp;
        return IstokenNotExpired;
    } catch (error) {
        console.log(error,'error');
        return error;
    }
}
export const refreshToken = async (req,res,next)=>{
    try {        
        const decodedrefresh = jwt.decode(req.body.refreshToken, process.env.JWT_SECRET);
        const user = await Users.findOne({_id:decodedrefresh._id});
        let authToken = jwt.sign({_id:user._id,role:user.role_id.name},process.env.JWT_SECRET,{
            expiresIn: '1h'
        });
        let refreshToken = jwt.sign({_id:user._id},process.env.JWT_SECRET,{
            expiresIn: '12h'
        });
        return res.status(200).json({authToken,refreshToken});
    } catch (error) {
        console.log(error);
        next(error);
    }
}
export const GetUserFromToken = async (token)=>{
    try {
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        let user = await Users.findById(decoded._id).populate("role_id").lean();
        return user;
    } catch (error) {
        console.log(error);
    }
}
export const createUser = async (req,res,next)=>{
    try{
    if(!validators.hasOwnProperty('createUser')){
        throw new Error (`${'createUser'} does not exist`);
    }
    await validators['createUser'].validateAsync(req.body,{ abortEarly: false });
    let user = req.body;
    let CreateUser = new Users(user);
    await CreateUser.save();
    return res.status(200).json({
        data:CreateUser,
        message:'User created'
    });
	}catch(err){
        if(err.name === 'ValidationError')
        console.log(Object.keys(err));
        if(err.code && err.code == 11000)
            return res.status(400).json({type:'mongoerror',message:'email aready taken'})
        if (err.isJoi)
            return res.status(400).json({error:err.details})
            else
        return res.status(500).json({message:'something went wrong'})
    }
}